<?php

namespace App\Http\Requests\Admin;

use A17\Twill\Http\Requests\Admin\Request;

class HomeRequest extends Request
{
    /**
     * @return array
     */
    public function rulesForCreate(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function rulesForUpdate(): array
    {
        return [];
    }
}
