<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class HomeController extends ModuleController
{
    protected $moduleName = 'home';
    protected $permalinkBase = 'home';
//    protected $indexOptions = [
//        'create' => true,
//        'edit' => true,
//        'publish' => true,
//        'bulkPublish' => true,
//        'feature' => false,
//        'bulkFeature' => false,
//        'restore' => true,
//        'bulkRestore' => true,
//        'forceDelete' => true,
//        'bulkForceDelete' => true,
//        'delete' => true,
//        'duplicate' => false,
//        'bulkDelete' => true,
//        'reorder' => false,
//        'permalink' => true,
//        'bulkEdit' => true,
//        'editInModal' => false,
//        'skipCreateModal' => false,
//    ];
}
