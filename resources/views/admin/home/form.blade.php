@extends('twill::layouts.form')

@section('contentFields')
    @formField('block_editor', [
    'blocks' => ['title', 'quote', 'text', 'image', 'grid', 'test', 'publications', 'news']
    ])
@stop
